<?php


namespace Cms\Template;


use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\TemplateWrapper;

class TemplatesRepository
{
    /**
     * @var Environment
     */
    private $environment;

    /**
     * TemplatesRepository constructor.
     * @param Environment $environment
     */
    public function __construct(Environment $environment)
    {
        $this->environment = $environment;
    }

    /**
     * @return TemplateWrapper
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getArticleTemplate(): TemplateWrapper
    {
        return $this->environment->load('Article.twig');
    }

    /**
     * @return TemplateWrapper
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getNewArticleTemplate(): TemplateWrapper
    {
        return $this->environment->load('NewArticle.twig');
    }

    /**
     * @return TemplateWrapper
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getArticlesListTemplate(): TemplateWrapper
    {
        return $this->environment->load('ArticlesList.twig');
    }

    /**
     * @return TemplateWrapper
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getErrorNotFoundTemplate(): TemplateWrapper
    {
        return $this->environment->load('ErrorNotFound.twig');
    }
}