<?php


namespace Cms\Controller;


use Cms\Exceptions\HydratorException;
use Cms\HttpResponse\HttpResponse;
use Cms\Model\Article;
use Cms\Repository\ArticleRepository;
use Cms\Template\TemplatesRepository;
use Twig\Error\Error;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ArticleController
{
    /**
     * @var Article[]
     */
    private $allArticles;
    /**
     * @var TemplatesRepository
     */
    private $templatesRepository;

    /**
     * ArticleController constructor.
     * @param ArticleRepository $articleRepository
     * @param TemplatesRepository $templatesRepository
     * @throws HydratorException
     */
    public function __construct(ArticleRepository $articleRepository, TemplatesRepository $templatesRepository)
    {
        $this->allArticles = $articleRepository->getAll();
        $this->templatesRepository = $templatesRepository;
    }

    public function list(): HttpResponse
    {
        try {
            $articles = [];
            foreach ($this->allArticles as $article) {
                $articles[] = ['title' => $article->getTitle(), 'content' => $article->getContent()];
            }
            $template = $this->templatesRepository->getArticlesListTemplate();
            return new HttpResponse(200, $template->render(['articles' => $articles]));
        } catch (Error $e) {
            // TODO: Log exception
            return $this->getServerErrorResponse();
        }
    }

    public function single(string $title): HttpResponse
    {
        $title = urldecode($title);
        try {
            $template = $this->templatesRepository->getArticleTemplate();
            foreach ($this->allArticles as $article) {
                if ($article->getTitle() === $title) {
                    return new HttpResponse(200, $template->render(['title' => $article->getTitle(), 'content' => $article->getContent()]));
                }
            }
            return new HttpResponse(404, 'Article "' . $title . '" not found.');
        } catch (Error $e) {
            // TODO: Log exception
            return $this->getServerErrorResponse();
        }
    }

    public function new(): HttpResponse
    {
        try {
            $template = $this->templatesRepository->getNewArticleTemplate();
        } catch (Error $e) {
            // TODO: Log exception
            return $this->getServerErrorResponse();
        }
        return new HttpResponse(200, $template->render());
    }

    private function getServerErrorResponse(): HttpResponse
    {
        return new HttpResponse(500, 'Server Error');
    }
}