<?php


namespace Cms\Model;


class Article implements IEntity
{
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $content;
    /**
     * @var int
     */
    private $id;

    /**
     * Article constructor.
     * @param int $id
     * @param string $title
     * @param string $content
     */
    public function __construct(int $id, string $title, string $content) // TODO: Add User $user to constructor parameters.
    {
        // TODO: Add parameters validation.
        $this->title = $title;
        $this->content = $content;
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }
}