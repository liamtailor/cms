<?php

namespace Cms\Model;

interface IEntity
{
    /**
     * @return int
     */
    public function getId(): int;
}