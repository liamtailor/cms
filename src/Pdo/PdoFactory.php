<?php

namespace Cms\Pdo;

use PDO;

class PdoFactory
{
    public function makePdo(string $user, string $password)
    {
        $dsn = 'mysql:host=localhost;dbname=cms;port=3307;charset=utf8mb4';
        return new PDO($dsn, $user, $password);
    }
}