<?php


namespace Cms\HttpResponse;


class HttpResponse
{
    /**
     * @var int
     */
    private $code;
    /**
     * @var string
     */
    private $content;

    public function __construct(int $code, string $content)
    {
        $this->code = $code;
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }
}