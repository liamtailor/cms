<?php


namespace Cms\Database;


use Cms\Exceptions\ViolatedUniqueConstraintException;
use Cms\Model\Article;
use PDO;

class DatabaseHandler
{
    /**
     * @var PDO
     */
    private $PDO;

    public function __construct(PDO $PDO)
    {
        $this->PDO = $PDO;
    }

    /**
     * @param Article $newArticle
     * @throws ViolatedUniqueConstraintException
     */
    public function saveArticle(Article $newArticle): void
    {
        foreach ($this->getAllArticles() as $article) {
            $articleTitle = $article['title'];
            if ($newArticle->getTitle() === $articleTitle)
                throw new ViolatedUniqueConstraintException('Article titled ' . $articleTitle . ' already exists in database.');
        }
        $sql = "INSERT INTO articles (title, content) VALUES (?, ?)";
        $statement = $this->PDO->prepare($sql);
        $statement->execute([$newArticle->getTitle(), $newArticle->getContent()]); // TODO: Sanitize inputs.
    }

    /**
     * @param Article $editedArticle
     * @param string $newTitle
     * @param string $newContent
     * @throws ViolatedUniqueConstraintException
     */
    public function editArticle(Article $editedArticle, string $newTitle, string $newContent): void
    {
        foreach ($this->getAllArticles() as $article) {
            $articleTitle = $article['title'];
            if ($newTitle === $articleTitle)
                throw new ViolatedUniqueConstraintException('Article titled ' . $articleTitle . ' already exists in database.');
        }
        $sql = "UPDATE articles SET title = ?, content = ? WHERE title = ?";
        $statement = $this->PDO->prepare($sql);
        $statement->execute([$newTitle, $newContent, $editedArticle->getTitle()]); // TODO: Sanitize inputs.
    }

    /**
     * @return Article[]
     */
    public function getAllArticles(): array
    {
        $sql = "SELECT id, title, content FROM articles";
        $statement = $this->PDO->query($sql);
        return $statement->fetchAll();
    }
}