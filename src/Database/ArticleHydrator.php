<?php


namespace Cms\Database;


use Cms\Exceptions\HydratorException;
use Cms\Model\Article;

class ArticleHydrator implements IEntityHydrator
{
    private const REQUIRED_ENTITY_KEYS = ['id', 'title', 'content'];

    /**
     * @param array $entities
     * @return Article[]
     * @throws HydratorException
     */
    public function hydrateAll(array $entities): array
    {
        $result = [];
        foreach ($entities as $article) {
            $this->validateArticle($article);
            $result[] = new Article($article['id'], $article['title'], $article['content']);
        }
        return $result;
    }

    /**
     * @param array $article
     * @throws HydratorException
     */
    private function validateArticle(array $article)
    {
        foreach (self::REQUIRED_ENTITY_KEYS as $requiredKey)
            if (!array_key_exists($requiredKey, $article))
                throw new HydratorException('Article doesn\'t have required field ' . $requiredKey . ' ' . print_r($article, true));
    }
}