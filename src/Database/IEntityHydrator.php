<?php

namespace Cms\Database;

use Cms\Model\IEntity;

interface IEntityHydrator
{
    /**
     * @param array $entities
     * @return IEntity[]
     */
    public function hydrateAll(array $entities): array;
}