<?php

namespace Cms\Repository;

interface IRepository
{
    public function getAll(): array;
}