<?php


namespace Cms\Repository;


use Cms\Database\ArticleHydrator;
use Cms\Database\DatabaseHandler;
use Cms\Exceptions\HydratorException;

class ArticleRepository implements IRepository
{
    /**
     * @var DatabaseHandler
     */
    private $databaseHandler;
    /**
     * @var ArticleHydrator
     */
    private $articleEntityHydrator;

    public function __construct(DatabaseHandler $databaseHandler, ArticleHydrator $articleEntityHydrator)
    {
        $this->databaseHandler = $databaseHandler;
        $this->articleEntityHydrator = $articleEntityHydrator;
    }

    /**
     * @return array
     * @throws HydratorException
     */
    public function getAll(): array
    {
        $articles = $this->databaseHandler->getAllArticles();
        return $this->articleEntityHydrator->hydrateAll($articles);
    }
}