<?php


namespace Cms\Repository;


use Cms\Database\ArticleHydrator;
use Cms\Database\DatabaseHandler;
use PDO;

class ArticleRepositoryFactory
{
    /**
     * @var PDO
     */
    private $PDO;

    public function __construct(PDO $PDO)
    {
        $this->PDO = $PDO;
    }

    public function makeArticleRepository()
    {
        return new ArticleRepository(new DatabaseHandler($this->PDO), new ArticleHydrator());
    }
}