<?php


use Cms\Database\ArticleHydrator;
use PHPUnit\Framework\TestCase;

class ArticleEntityHydratorTest extends TestCase
{

    /**
     * @var ArticleHydrator
     */
    private $hydrator;

    public function setUp(): void
    {
        $this->hydrator = new ArticleHydrator();
    }

    /**
     * @throws \Cms\Exceptions\HydratorException
     */
    public function testHydrateAll_ReturnsValidArticles()
    {
        $expectedIds = [1, 2];
        $expectedTitles = ['test title one', 'title two'];
        $expectedContents = ['test content one', 'content two'];
        $articles = [
            ['id' => $expectedIds[0], 'title' => $expectedTitles[0], 'content' => $expectedContents[0]],
            ['id' => $expectedIds[1], 'title' => $expectedTitles[1], 'content' => $expectedContents[1]],
        ];

        $actual = $this->hydrator->hydrateAll($articles);

        self::assertIsArray($actual);
        self::assertCount(2, $actual);
        foreach ($actual as $article) {
            self::assertInstanceOf(\Cms\Model\Article::class, $article);
        }
        self::assertEquals($expectedIds[0], $actual[0]->getId());
        self::assertEquals($expectedTitles[0], $actual[0]->getTitle());
        self::assertEquals($expectedContents[0], $actual[0]->getContent());
        self::assertEquals($expectedIds[1], $actual[1]->getId());
        self::assertEquals($expectedTitles[1], $actual[1]->getTitle());
        self::assertEquals($expectedContents[1], $actual[1]->getContent());
    }

    /**
     * @dataProvider invalid_article_provider
     * @param array $article
     * @param string $expectedExceptionPartialMessage
     */
    public function testHydrateAll_ThrowsHydratorExceptionOnMissingArticleField(array $article, string $expectedExceptionPartialMessage)
    {
        self::expectException(\Cms\Exceptions\HydratorException::class);
        self::expectExceptionMessage($expectedExceptionPartialMessage);

        $this->hydrator->hydrateAll([$article]);
    }

    public function invalid_article_provider()
    {
        $expectedMessagePart = 'Article doesn\'t have required field ';
        return [
            [['title' => 'test title', 'content' => 'test content'], $expectedMessagePart . 'id'],
            [['id' => 1, 'content' => 'test content'], $expectedMessagePart . 'title'],
            [['id' => 1, 'title' => 'test title'], $expectedMessagePart . 'content']
        ];
    }
}
