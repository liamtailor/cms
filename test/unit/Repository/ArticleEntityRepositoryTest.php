<?php


use Cms\Database\ArticleHydrator;
use Cms\Database\DatabaseHandler;
use Cms\Model\Article;
use Cms\Repository\ArticleRepository;
use PHPUnit\Framework\TestCase;

class ArticleEntityRepositoryTest extends TestCase
{

    public function testGetAll_ReturnsValidArrayOfArticles()
    {
        $expected = [$this->createMock(Article::class), $this->createMock(Article::class)];
        $articles = ['articleOne', 'articleTwo'];
        $databaseHandler = $this->createMock(DatabaseHandler::class);
        $databaseHandler->method('getAllArticles')->willReturn($articles);
        $articleEntityHydrator = $this->createMock(ArticleHydrator::class);
        $articleEntityHydrator->method('hydrateAll')->with($articles)->willReturn($expected);
        $repository = new ArticleRepository($databaseHandler, $articleEntityHydrator);
        
        $actual = $repository->getAll();
        
        self::assertSame($expected, $actual);
    }
}
