<?php
require_once __DIR__ . '/bootstrap.php';

use Cms\Database\DatabaseHandler;
use Cms\Exceptions\ViolatedUniqueConstraintException;
use Cms\Model\Article;
use Cms\Repository\ArticleRepositoryFactory;

$currentTitle = $_GET['current_title'];
$newTitle = $_GET['title'];
$newContent = $_GET['content'];

$databaseHandler = new DatabaseHandler($pdo);
$redirectLocation = "http://" . $_SERVER['HTTP_HOST'];
$articleRepository = (new ArticleRepositoryFactory($pdo))->makeArticleRepository();
try {
    /** @var Article $article */
    foreach ($articleRepository->getAll() as $article){
        if ($article->getTitle() === $currentTitle) {
            $currentArticle = $article;
            break;
        }
    }
    $databaseHandler->editArticle($currentArticle, $newTitle, $newContent);
} catch (ViolatedUniqueConstraintException $e) {
    echo "<a href=" . $redirectLocation . "/new-article" . ">GO BACK</a>";
    echo "<br/>";
    echo "ERROR: " . $e->getMessage();
    exit();
}

header("Location: " . $redirectLocation . "/");
exit();
