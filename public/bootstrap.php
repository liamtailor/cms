<?php
require_once __DIR__ . '/../vendor/autoload.php';

use Cms\Pdo\PdoFactory;
use Cms\Template\TemplatesRepository;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

$loader = new FilesystemLoader(__DIR__ . '/../src/Template/'); // TODO: Validate all of these files exist
$twig = new Environment($loader, []);
$templatesRepository = new TemplatesRepository($twig);
$pdoFactory = new PdoFactory();

// db connection
$databaseUser = file_get_contents(__DIR__ . '/../config/databaseUser.txt');
$databasePassword = file_get_contents(__DIR__ . '/../config/databasePassword.txt');
if ($databasePassword === false || $databaseUser === false) {
    throw new Exception('Couldn\'t load database user or password');
}
$pdo = $pdoFactory->makePdo($databaseUser, $databasePassword);