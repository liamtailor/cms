<?php
require_once __DIR__ . '/bootstrap.php';

use Cms\Database\DatabaseHandler;
use Cms\Exceptions\ViolatedUniqueConstraintException;
use Cms\Model\Article;

$title = $_GET['title'];
$content = $_GET['content'];

$databaseHandler = new DatabaseHandler($pdo);
$redirectLocation = "http://" . $_SERVER['HTTP_HOST'];
try {
    $databaseHandler->saveArticle(new Article(1, $title, $content));
} catch (ViolatedUniqueConstraintException $e) {
    echo "<a href=" . $redirectLocation . "/new-article" . ">GO BACK</a>";
    echo "<br/>";
    echo "ERROR: " . $e->getMessage();
    exit();
}

header("Location: " . $redirectLocation . "/");
exit();
