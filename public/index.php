<?php
require_once __DIR__ . '/bootstrap.php';

use Cms\Controller\ArticleController;
use Cms\Repository\ArticleRepositoryFactory;
use Cms\Template\TemplatesRepository;

try {
    $articleRepository = (new ArticleRepositoryFactory($pdo))->makeArticleRepository();

    $controller = new ArticleController($articleRepository, new TemplatesRepository($twig));
    if ($_SERVER['REQUEST_URI'] === '/new-article') { // TODO: Router
        $response = $controller->new();
    } else {
        if (array_key_exists('title', $_GET)) {
            $articleTitle = urldecode($_GET['title']);
            $response = $controller->single($articleTitle);
        } else {
            $response = $controller->list();
        }
    }

    http_response_code($response->getCode());
    if ($response->getCode() === 404) {
        echo $templatesRepository->getErrorNotFoundTemplate()->render(['errorMessage' => $response->getContent()]);
    } else {
        echo $response->getContent();
    }
} catch (Exception $e) {
    // TODO: Log exception
    http_response_code(500);
    echo "SERVER ERROR 500";
}
?>


